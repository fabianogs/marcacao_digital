﻿namespace CoopertranscBiometria
{
    partial class Coopertransc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Coopertransc));
            this.btnRegistrarDigital = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblCooperado = new System.Windows.Forms.Label();
            this.btnFechar = new System.Windows.Forms.Button();
            this.but_config = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRegistrarDigital
            // 
            this.btnRegistrarDigital.Location = new System.Drawing.Point(268, 15);
            this.btnRegistrarDigital.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRegistrarDigital.Name = "btnRegistrarDigital";
            this.btnRegistrarDigital.Size = new System.Drawing.Size(137, 28);
            this.btnRegistrarDigital.TabIndex = 2;
            this.btnRegistrarDigital.Text = "Registrar Digital";
            this.btnRegistrarDigital.UseVisualStyleBackColor = true;
            this.btnRegistrarDigital.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(52, 450);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(541, 398);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // lblCooperado
            // 
            this.lblCooperado.AutoSize = true;
            this.lblCooperado.Location = new System.Drawing.Point(432, 82);
            this.lblCooperado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCooperado.Name = "lblCooperado";
            this.lblCooperado.Size = new System.Drawing.Size(0, 17);
            this.lblCooperado.TabIndex = 9;
            this.lblCooperado.Visible = false;
            // 
            // btnFechar
            // 
            this.btnFechar.Location = new System.Drawing.Point(16, 15);
            this.btnFechar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(100, 28);
            this.btnFechar.TabIndex = 10;
            this.btnFechar.Text = "Fechar ";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // but_config
            // 
            this.but_config.Location = new System.Drawing.Point(124, 15);
            this.but_config.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.but_config.Name = "but_config";
            this.but_config.Size = new System.Drawing.Size(136, 28);
            this.but_config.TabIndex = 12;
            this.but_config.Text = "Configurações";
            this.but_config.UseVisualStyleBackColor = true;
            this.but_config.Click += new System.EventHandler(this.but_config_Click);
            // 
            // Coopertransc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1344, 897);
            this.Controls.Add(this.but_config);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.lblCooperado);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnRegistrarDigital);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Coopertransc";
            this.Text = "Coopertransc - Marcação de Vez Biométrica";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Coopertransc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnRegistrarDigital;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblCooperado;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button but_config;
    }
}

