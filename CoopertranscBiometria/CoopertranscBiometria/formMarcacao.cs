﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoopertranscBiometria
{
    public partial class formMarcacao : Form
    {
        private string url;

        public formMarcacao(string u)
        {
            this.url = u;
            InitializeComponent();
        }

        private void formMarcacao_Load(object sender, EventArgs e)
        {
            webBrowser1.Url = new Uri(url);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
