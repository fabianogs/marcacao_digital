﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DPUruNet;
using System.Drawing.Imaging;

namespace CoopertranscBiometria
{
    public partial class Coopertransc : Form
    {
        private ReaderCollection _readers;
        public Reader reader;
        private const int DPFJ_PROBABILITY_ONE = 0x7fffffff;

        private DateTime controlMessageTime = DateTime.Now;

        private static string lnk;

        private bool onEnroll;
        private string cpf;
        private string nome;
        List<Fmd> preenrollmentFmds;
        List<Fmd> enrollmentFmds;
        List<string> enrollmentMap;

        private string url_bd;
        private string nome_bd;
        private string usuario_bd;
        private string senha;
        private string senha_cadastro;
        private string senha_fechar;
        private string senha_config;
        private string url;


        public Coopertransc()
        {
            InitializeComponent();
            senha_cadastro = Properties.Settings.Default.senha_cadastro;
            senha_fechar = Properties.Settings.Default.senha_fechar;
            url = Properties.Settings.Default.url;
            senha_config = Properties.Settings.Default.senha_config;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string input = Microsoft.VisualBasic.Interaction.InputBox("Qual é a senha?", "Registro de Digital");
            if (input.Equals(senha_cadastro))
            {
                this.cpf = Microsoft.VisualBasic.Interaction.InputBox("Digite o CPF do cooperado para registro", "Registro de Digital");

                dao db = new CoopertranscBiometria.dao();
                this.nome = db.getCooperado(cpf);

                if (nome.Equals("não"))
                {
                    MessageBox.Show("Não encontrado. Nada feito","Erro na procura");
                }
                else
                {
                    string saida = "Cooperado: " + this.nome + ". Confirma?";

                    if (MessageBox.Show(saida, "Confirmação do cooperado para registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        onEnroll = true;
                        MessageBox.Show("Iniciando o registro da digital, coloque o dedo no leitor biométrico");
                    }
                }
            }
            else
                MessageBox.Show("Senha errada!");
        }

        private void Coopertransc_Load(object sender, EventArgs e)
        {
            onEnroll = false;
            preenrollmentFmds = new List<Fmd>();

            try
            {
                _readers = ReaderCollection.GetReaders();
            }
            catch (Exception)
            {
                MessageBox.Show("Não foi possível detectar o leitor");
            }

            foreach (Reader _reader in _readers)
            {
                reader = _reader;
                break;
            }

            if (reader == null)
            {
                MessageBox.Show("Nenhum leitor conectado!");
                return;
            }

            // Inicializar o Sensor
            DPUruNet.Constants.ResultCode result = DPUruNet.Constants.ResultCode.DP_DEVICE_FAILURE;
            result = reader.Open(DPUruNet.Constants.CapturePriority.DP_PRIORITY_COOPERATIVE);
            if (result != DPUruNet.Constants.ResultCode.DP_SUCCESS)
            {
                MessageBox.Show("Erro ao iniciar o sensor!");
                return;
            }

            // Definir callback assincrono
            try
            {
                reader.On_Captured += new Reader.CaptureCallback(this.OnCaptured);
                DPUruNet.Constants.ResultCode captureResult = reader.CaptureAsync(
                    DPUruNet.Constants.Formats.Fid.ANSI,
                    DPUruNet.Constants.CaptureProcessing.DP_IMG_PROC_DEFAULT,
                    reader.Capabilities.Resolutions[0]
                );
                if (captureResult != DPUruNet.Constants.ResultCode.DP_SUCCESS)
                {
                    throw new Exception("" + captureResult);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao definir callback: " + ex.Message);
            }

            if (!APIInitialData())
            {
                reader.Dispose();
                return;
            }
            btnRegistrarDigital.Enabled = true;
            return;
        }

        private Boolean APIInitialData()
        {
            enrollmentFmds = new List<Fmd>();
            enrollmentMap = new List<string>();
            dao db = new dao();
            List<Cooperado> cooperados = new List<Cooperado>();
            cooperados = db.Select();

            if (cooperados != null)
            {
                foreach (var val in cooperados)
                {
                    try
                    {
                        enrollmentFmds.Add(DPUruNet.Fmd.DeserializeXml(val.Imagem));
                        enrollmentMap.Add(val.Cpf);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        continue;
                    }
                }

                return true;
            }
            else
                return true;
        }

        public void OnCaptured(CaptureResult captureResult)
        {
            try
            {
                // Check capture quality and throw an error if bad.
                if (!this.CheckCaptureResult(captureResult))
                {
                    MessageBox.Show("Qualidade da imagem ruim, tente novamente!");
                    return;
                }

                // Create bitmap
                foreach (Fid.Fiv fiv in captureResult.Data.Views)
                {
                    Bitmap img = CreateBitmap(fiv.RawImage, fiv.Width, fiv.Height);
                    pictureBox1.Image = (Bitmap)img;
                    pictureBox1.Refresh();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //MessageBox.Show("Erro ao capturar digital! Erro: " + ex.Message);
            }

            DataResult<Fmd> resultConversion = FeatureExtraction.CreateFmdFromFid(
                captureResult.Data,
                DPUruNet.Constants.Formats.Fmd.ANSI
            );
            if (resultConversion.ResultCode != DPUruNet.Constants.ResultCode.DP_SUCCESS)
            {
                MessageBox.Show("Erro no processamento. Coloque novamente o dedo! Erro: " + resultConversion.ResultCode.ToString());
                return;
            }

            if (!onEnroll)
            {
                int thresholdScore = DPFJ_PROBABILITY_ONE * 1 / 100000;

                IdentifyResult identifyResult = Comparison.Identify(resultConversion.Data, 0, enrollmentFmds, thresholdScore, 1);
                if (identifyResult.ResultCode != DPUruNet.Constants.ResultCode.DP_SUCCESS)
                {
                    Console.WriteLine(identifyResult.ResultCode.ToString());
                }

                if (identifyResult.Indexes.Length == 0)
                {
                    MessageBox.Show("Digital não identificada! Checkin não realizado!");
                    return;
                }
                //MessageBox.Show("Enviando checkin ao servidor!");
                APICheckin(enrollmentMap[identifyResult.Indexes[0][0]]);
            }
            else
            {
                preenrollmentFmds.Add(resultConversion.Data);

                if (preenrollmentFmds.Count() < 4)
                {
                    MessageBox.Show("Digital capturada com sucesso. Clique OK para continuar e depois coloque o dedo novamente!\nRestantes:"+(4- preenrollmentFmds.Count()));
                }
                else
                {
                    DataResult<Fmd> resultEnrollment = DPUruNet.Enrollment.CreateEnrollmentFmd(DPUruNet.Constants.Formats.Fmd.ANSI, preenrollmentFmds);

                    if (resultEnrollment.ResultCode == DPUruNet.Constants.ResultCode.DP_SUCCESS)
                    {
                        if (APIRegister(cpf, DPUruNet.Fmd.SerializeXml(resultEnrollment.Data)))
                        {
                            MessageBox.Show("Digital registrada com sucesso", "Digital registrada ", MessageBoxButtons.OK);
                            pictureBox1.Image = null;
                        }
                        else
                        {
                            MessageBox.Show("Erro ao Registrar a Digital", "Digital não registrada", MessageBoxButtons.OK);
                        }
                        preenrollmentFmds.Clear();
                        onEnroll = false;
                        cpf = null;
                        return;
                    }
                    else
                    {
                        MessageBox.Show("As digitais não conferem! Digital não registrada!", "Digital não registrada", MessageBoxButtons.OK);
                        preenrollmentFmds.Clear();
                        onEnroll = false;
                        cpf = null;
                        return;
                    }
                }
            }
        }

        
        private void APICheckin(string user_id)
        {
            string url2 = url + "index.php?cpf=" + user_id;
            formMarcacao marca = new formMarcacao(url2);
            marca.ShowDialog();
            pictureBox1.Image = null;
            url2 = "";
        }
        

        public bool CheckCaptureResult(CaptureResult captureResult)
        {
            if (captureResult.Data == null || captureResult.ResultCode != DPUruNet.Constants.ResultCode.DP_SUCCESS)
            {
                if (captureResult.ResultCode != DPUruNet.Constants.ResultCode.DP_SUCCESS)
                {
                    throw new Exception(captureResult.ResultCode.ToString());
                }

                // Send message if quality shows fake finger
                if ((captureResult.Quality != DPUruNet.Constants.CaptureQuality.DP_QUALITY_CANCELED))
                {
                    throw new Exception("Quality - " + captureResult.Quality);
                }
                return false;
            }

            return true;
        }

        private Boolean APIRegister(string cpf, string tpl)
        {
            try
            {
                dao db = new CoopertranscBiometria.dao();
                db.Update(cpf, tpl);
            }
            catch (Exception)
            {
                MessageBox.Show("Erro no conteúdo retornado pelo servidor! Digital não registrada!", "Digital não registrada", MessageBoxButtons.OK);
                return false;
            }
            // Recarregar dados
            APIInitialData();
            return true;
        }
    
        public Bitmap CreateBitmap(byte[] bytes, int width, int height)
        {
            byte[] rgbBytes = new byte[bytes.Length * 3];

            for (int i = 0; i <= bytes.Length - 1; i++)
            {
                rgbBytes[(i * 3)] = bytes[i];
                rgbBytes[(i * 3) + 1] = bytes[i];
                rgbBytes[(i * 3) + 2] = bytes[i];
            }
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            BitmapData data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            for (int i = 0; i <= bmp.Height - 1; i++)
            {
                IntPtr p = new IntPtr(data.Scan0.ToInt64() + data.Stride * i);
                System.Runtime.InteropServices.Marshal.Copy(rgbBytes, i * bmp.Width * 3, p, bmp.Width * 3);
            }

            bmp.UnlockBits(data);

            return bmp;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            string input = Microsoft.VisualBasic.Interaction.InputBox("Qual é a senha para sair?", "Registro de Digital");
            if (input.Equals(senha_fechar))
                System.Windows.Forms.Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            formConfig teste = new formConfig();
            teste.Show();
        }

        private void but_config_Click(object sender, EventArgs e)
        {
            string input = Microsoft.VisualBasic.Interaction.InputBox("Qual é a senha?", "Configurações");
            if (input.Equals(senha_config))
            {
                formConfig frm = new formConfig();
                frm.Show();
            }
            else
                MessageBox.Show("Senha errada!");
        }
    }//fim class
}
