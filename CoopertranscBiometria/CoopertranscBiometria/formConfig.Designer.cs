﻿namespace CoopertranscBiometria
{
    partial class formConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.butTeste = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.butGravar = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_senha_fechar = new System.Windows.Forms.TextBox();
            this.txt_senha_registro = new System.Windows.Forms.TextBox();
            this.txt_senha_config = new System.Windows.Forms.TextBox();
            this.cmbTeste = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tipTeste = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // butTeste
            // 
            this.butTeste.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butTeste.Location = new System.Drawing.Point(547, 168);
            this.butTeste.Margin = new System.Windows.Forms.Padding(4);
            this.butTeste.Name = "butTeste";
            this.butTeste.Size = new System.Drawing.Size(203, 62);
            this.butTeste.TabIndex = 8;
            this.butTeste.Text = "Testar conexão";
            this.butTeste.UseVisualStyleBackColor = true;
            this.butTeste.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 256);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Mensagem";
            // 
            // butGravar
            // 
            this.butGravar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butGravar.Location = new System.Drawing.Point(288, 168);
            this.butGravar.Margin = new System.Windows.Forms.Padding(4);
            this.butGravar.Name = "butGravar";
            this.butGravar.Size = new System.Drawing.Size(251, 62);
            this.butGravar.TabIndex = 10;
            this.butGravar.Text = "Gravar config";
            this.butGravar.UseVisualStyleBackColor = true;
            this.butGravar.Click += new System.EventHandler(this.button2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(94, 20);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(173, 25);
            this.label6.TabIndex = 11;
            this.label6.Text = "Senha para fechar";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(17, 57);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(244, 25);
            this.label7.TabIndex = 12;
            this.label7.Text = "Senha para registrar digital";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(21, 94);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(241, 25);
            this.label8.TabIndex = 13;
            this.label8.Text = "Senha para configurações";
            // 
            // txt_senha_fechar
            // 
            this.txt_senha_fechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_senha_fechar.Location = new System.Drawing.Point(290, 17);
            this.txt_senha_fechar.Margin = new System.Windows.Forms.Padding(4);
            this.txt_senha_fechar.Name = "txt_senha_fechar";
            this.txt_senha_fechar.Size = new System.Drawing.Size(460, 30);
            this.txt_senha_fechar.TabIndex = 14;
            // 
            // txt_senha_registro
            // 
            this.txt_senha_registro.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_senha_registro.Location = new System.Drawing.Point(290, 54);
            this.txt_senha_registro.Margin = new System.Windows.Forms.Padding(4);
            this.txt_senha_registro.Name = "txt_senha_registro";
            this.txt_senha_registro.Size = new System.Drawing.Size(460, 30);
            this.txt_senha_registro.TabIndex = 15;
            // 
            // txt_senha_config
            // 
            this.txt_senha_config.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_senha_config.Location = new System.Drawing.Point(290, 90);
            this.txt_senha_config.Margin = new System.Windows.Forms.Padding(4);
            this.txt_senha_config.Name = "txt_senha_config";
            this.txt_senha_config.Size = new System.Drawing.Size(460, 30);
            this.txt_senha_config.TabIndex = 16;
            // 
            // cmbTeste
            // 
            this.cmbTeste.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTeste.FormattingEnabled = true;
            this.cmbTeste.Items.AddRange(new object[] {
            "SIM",
            "NÃO"});
            this.cmbTeste.Location = new System.Drawing.Point(290, 127);
            this.cmbTeste.Name = "cmbTeste";
            this.cmbTeste.Size = new System.Drawing.Size(121, 33);
            this.cmbTeste.TabIndex = 19;
            this.tipTeste.SetToolTip(this.cmbTeste, "No modo teste será usado o banco de dados COOPERTESTES e o AMBIENTE DE TESTE (int" +
        "ranet_teste).\r\nAntes de testar, grave as configurações.");
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(153, 127);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(109, 25);
            this.label10.TabIndex = 20;
            this.label10.Text = "Modo teste";
            // 
            // tipTeste
            // 
            this.tipTeste.ToolTipTitle = "Modo teste";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(424, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(329, 17);
            this.label1.TabIndex = 21;
            this.label1.Text = "Antes de testar a conexão, grave as configurações";
            // 
            // formConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 375);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cmbTeste);
            this.Controls.Add(this.txt_senha_config);
            this.Controls.Add(this.txt_senha_registro);
            this.Controls.Add(this.txt_senha_fechar);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.butGravar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.butTeste);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "formConfig";
            this.Text = "Configurações gerais";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button butTeste;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button butGravar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_senha_fechar;
        private System.Windows.Forms.TextBox txt_senha_registro;
        private System.Windows.Forms.TextBox txt_senha_config;
        private System.Windows.Forms.ComboBox cmbTeste;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ToolTip tipTeste;
        private System.Windows.Forms.Label label1;
    }
}