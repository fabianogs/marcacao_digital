﻿using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Resources;

namespace CoopertranscBiometria
{
    internal class dao
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;
        private string senha_registro;
        private string senha_sair;
        private string url;

        public dao()
        {
            Initialize();
        }

        private void Initialize()
        {
            carregaParametros();
            string connectionString;
            connectionString = "Server=" + this.server + ";" + "DataBase=" + this.database + ";" + "Uid=" + this.uid + ";" + "Pwd=" + this.password;
            connection = new MySqlConnection(connectionString);
        }

        public void carregaParametros()
        {
            server = Properties.Settings.Default.url_bd;
            database = Properties.Settings.Default.nome_bd;
            uid = Properties.Settings.Default.usuario_bd;
            password = Properties.Settings.Default.senha;
        }

        private bool OpenConnection()
        {
            try
            {
                if (!(connection.State == System.Data.ConnectionState.Open))
                {
                    connection.Open();
                    return true;
                }
                else
                    return false;
                
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public void Update(string cpf, string imagem) {
            string query = "UPDATE cooperado SET digital = '"+imagem+"' where cpf='"+cpf+"'";

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = query;
                cmd.Connection = connection;

                cmd.ExecuteNonQuery();
                this.CloseConnection();
            }
        }

        public string getCooperado(string cpf)
        {
            string query = "select nome from cooperado where cpf='" + cpf + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    string _nome = (string)dataReader["nome"];
                    return _nome;
                }
                else
                {
                    return "não";
                }
                
            }
            else
                return "Não encontrado";
        }

        public List<Cooperado> Select() {
            string query = "select cpf, digital from cooperado where ativo='S' and digital<>''";
            List<Cooperado> list = new List<Cooperado>();

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    Cooperado c = new Cooperado();
                    c.Cpf = (string)dataReader["cpf"];
                    c.Imagem = (string)dataReader["digital"];
                    list.Add(c);
                    c = null;
                }
                dataReader.Close();
                this.CloseConnection();
                return list;
            }
            else
                return list;
        }

        public int Count()
        {
            string query = "SELECT Count(*) FROM cooperado where ativo='S' and digital<>''";
            int Count = -1;

            if (this.OpenConnection() == true)
            {
                //Create Mysql Command
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //ExecuteScalar will return one value
                Count = int.Parse(cmd.ExecuteScalar() + "");
                            
                //close Connection
                this.CloseConnection();
                MessageBox.Show(Count.ToString());
                return Count;
            }
            else
            {
                return Count;
            }
        }
    }
}