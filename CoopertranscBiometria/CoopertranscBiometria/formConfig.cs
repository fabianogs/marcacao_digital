﻿using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;

namespace CoopertranscBiometria
{
    public partial class formConfig : Form
    {
        private MySqlConnection connection;

        public formConfig()
        {
            InitializeComponent();
            txt_senha_registro.Text = Properties.Settings.Default.senha_cadastro;
            txt_senha_config.Text = Properties.Settings.Default.senha_config;
            txt_senha_fechar.Text = Properties.Settings.Default.senha_fechar;
            cmbTeste.SelectedItem = Properties.Settings.Default.teste;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenConnection();
        }

        private void OpenConnection()
        {
            string connection_string,
                txt_url_intranet = Properties.Settings.Default.url,
                txt_host = Properties.Settings.Default.url_bd,
                txt_usuario = Properties.Settings.Default.usuario_bd,
                txt_banco = Properties.Settings.Default.nome_bd,
                txt_senha = Properties.Settings.Default.senha;

            connection_string = "Server=" + txt_host + ";" + "DataBase=" + txt_banco + ";" + "Uid=" + txt_usuario + ";" + "Pwd=" + txt_senha;
            connection = new MySqlConnection(connection_string);
            label5.Text = connection_string;
            var command = connection.CreateCommand();
            try
            {
                connection.Open();
                command.CommandText = "select * from usuarios";
                command.ExecuteNonQuery();
                if (connection.State.ToString() == "Open")
                {
                    label5.Text = "Conexão com o banco " + txt_banco.ToUpper() + " OK";
                }
            }
            catch (Exception ex)
            {
                label5.Text = ex.ToString();
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if ((string)cmbTeste.SelectedItem == "SIM")
            {
                Properties.Settings.Default.url = "http://www.coopertransc.com.br/intranet_teste/biomarcacao/";
                Properties.Settings.Default.url_bd = "coopertestes.mysql.dbaas.com.br";
                Properties.Settings.Default.usuario_bd = "coopertestes";
                Properties.Settings.Default.nome_bd = "coopertestes";
                Properties.Settings.Default.senha = "C00p3r*17";
            }
            else
            {
                Properties.Settings.Default.url = "http://www.coopertransc.com.br/intranet/biomarcacao/";
                Properties.Settings.Default.url_bd = "mysql02.coopertransc.com.br";
                Properties.Settings.Default.usuario_bd = "coopertransc2";
                Properties.Settings.Default.nome_bd = "coopertransc2";
                Properties.Settings.Default.senha = "ctacsc0420";
            }

            Properties.Settings.Default.senha_cadastro = txt_senha_registro.Text;
            Properties.Settings.Default.senha_config = txt_senha_config.Text;
            Properties.Settings.Default.senha_fechar = txt_senha_fechar.Text;
            Properties.Settings.Default.teste = (string) cmbTeste.SelectedItem;
            Properties.Settings.Default.Save();
        }
    }
}
