﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoopertranscBiometria
{
    class Cooperado
    {
        private string cpf;
        private string imagem;

        public string Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        }

        public string Imagem
        {
            get { return imagem; }
            set { imagem = value; }
        }

    }
}
